import React from "react";
import { Link as InternalLink } from "react-router-dom";

export function Link(props: {
    to: string;
    children: JSX.Element | JSX.Element[];
    className?: string;
    style?: React.StyleHTMLAttributes<HTMLElement>;
    onClick?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
}) {
    const { to, children, className, style, onClick } = props;

    if (to.match(/^http(s|):\/\//))
        return (
            <a href={to} className={className} style={style} onClick={onClick}>
                {children}
            </a>
        );
    else
        return (
            <InternalLink to={to} className={className} style={style} onClick={onClick}>
                {children}
            </InternalLink>
        );
}
