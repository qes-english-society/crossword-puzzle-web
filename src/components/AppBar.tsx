/*
The MIT License (MIT)

Copyright (c) 2014 Call-Em-All

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import { useUser } from "../context/AppContext";
import { useLogout } from "../hooks/logout";
import { BarChart, Code, Home } from "@mui/icons-material";

export default function ResponsiveAppBar() {
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

    const [user] = useUser();

    const logout = useLogout();

    const btns = [
        { title: "Home", link: "/", icon: <Home className={"mr5"} /> },
        !user && { title: "Sign in", link: "/users/signin" },
        !user && { title: "Register", link: "/users/register" },
        user && { title: "Log out", action: logout },
        { title: "Rankings", link: "/rank", icon: <BarChart className={"mr5"} /> },
        {
            title: "Source code",
            link: "https://gitlab.com/qes-english-society/crossword-puzzle",
            icon: <Code className={"mr5"} />,
        },
    ];

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <AppBar sx={{ position: "initial" }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        variant="h6"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: "none", md: "flex" },
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    >
                        QES English Society
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "left",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "left",
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: "block", md: "none" },
                            }}
                        >
                            {btns.map(
                                (btn) =>
                                    btn && (
                                        <MenuItem
                                            to={btn.link || "#"}
                                            href={btn.link || "#"}
                                            component={
                                                btn.link?.match(/^http(s|):\/\//)
                                                    ? "a"
                                                    : Link
                                            }
                                            key={btn.title}
                                            onClick={() => {
                                                btn.action && btn.action();
                                                handleCloseNavMenu();
                                            }}
                                        >
                                            {btn.icon}
                                            <Typography
                                                textAlign="center"
                                                className={"black-force"}
                                            >
                                                {btn.title}
                                            </Typography>
                                        </MenuItem>
                                    )
                            )}
                        </Menu>
                    </Box>
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { xs: "flex", md: "none" },
                            flexGrow: 1,
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    >
                        QES English Society
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                        {btns.map(
                            (btn) =>
                                btn && (
                                    <Button
                                        to={btn.link || "#"}
                                        href={btn.link || "#"}
                                        component={
                                            btn.link?.match(/^http(s|):\/\//) ? "a" : Link
                                        }
                                        key={btn.title}
                                        className={"black-force"}
                                        onClick={() => {
                                            btn.action && btn.action();
                                            handleCloseNavMenu();
                                        }}
                                    >
                                        {btn.icon}
                                        {btn.title}
                                    </Button>
                                )
                        )}
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
}
