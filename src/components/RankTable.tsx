import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@mui/material";
import humanizeDuration from "humanize-duration";
import { puzzle, ranking } from "../pages/Rankings";
import { useIsSmallScreen } from "../context/AppContext";

export default function RankTable(props: {
    data: ranking[];
    puzzles: puzzle[];
    showPuzzleName?: boolean;
}) {
    const { data, puzzles, showPuzzleName } = props;
    const isSmallScreen = useIsSmallScreen();
    return (
        <TableContainer component={Paper} sx={{ width: isSmallScreen ? "90vw" : "60vw" }}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Rank</TableCell>
                        {showPuzzleName && <TableCell>Puzzle</TableCell>}
                        <TableCell>Class</TableCell>
                        <TableCell>Class No.</TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Unsolved</TableCell>
                        <TableCell>Time</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, index) => (
                        <TableRow
                            key={row.email}
                            sx={{
                                "&:last-child td, &:last-child th": { border: 0 },
                            }}
                        >
                            <TableCell component="th" scope="row">
                                {index + 1}
                            </TableCell>
                            {showPuzzleName && (
                                <TableCell component="th" scope={"row"}>
                                    {
                                        puzzles.find((p) => p.id === row.result.id)
                                            ?.short_topic
                                    }
                                </TableCell>
                            )}
                            <TableCell>{row.class_name}</TableCell>
                            <TableCell>{row.class_number}</TableCell>
                            <TableCell component="th" scope="row">
                                {row.email}
                            </TableCell>
                            <TableCell>{row.result.unsolved}</TableCell>
                            <TableCell>
                                {humanizeDuration(row.result.time, { round: true })}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
