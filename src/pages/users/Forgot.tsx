import { Alert, Box, Button, CircularProgress, TextField } from "@mui/material";
import { useIsSmallScreen, useUser } from "../../context/AppContext";
import { FormEvent, useState } from "react";
import { Navigate } from "react-router-dom";
import { api } from "../../lib/api";

export default function Forgot() {
    const [alert, setAlert] = useState<{
        severity: "success" | "error";
        message: string;
    }>({
        severity: "success",
        message: "",
    });
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(false);
    const [user] = useUser();
    const [email, setEmail] = useState("");

    const isSmallScreen = useIsSmallScreen();

    if (user) return <Navigate to={"/"} replace />;

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        setLoading(true);
        setError("");
        api.post("/users/forgot", {
            email,
        })
            .then(() => {
                setLoading(false);
                setAlert({
                    severity: "success",
                    message:
                        "Check your email for instructions on resetting your password.",
                });
            })
            .catch((err) => {
                setLoading(false);
                setAlert({
                    severity: "error",
                    message:
                        err?.response?.data?.error ||
                        err?.response?.data ||
                        "An error occurred. Please try again later.",
                });
            });
    };

    return (
        <Box className={"flex align-center flex-dir-column mb40"}>
            <form
                onSubmit={onSubmit}
                className={"flex flex-dir-column"}
                style={{ width: isSmallScreen ? "80vw" : "50vw" }}
            >
                <h2>Forgot password</h2>
                {alert.message && (
                    <Alert className={"mb20"} severity={alert.severity}>
                        {alert.message}
                    </Alert>
                )}
                <TextField
                    className={"mb20"}
                    label={`Email`}
                    type={"email"}
                    inputProps={{
                        pattern: "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk",
                    }}
                    onChange={(e) => {
                        setEmail(e.currentTarget.value);
                    }}
                    helperText={
                        !email.match(
                            /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk$/
                        )
                            ? "Email must end with cloud.qes.edu.hk"
                            : ""
                    }
                    error={!!error}
                    disabled={loading}
                    variant={"outlined"}
                    required
                    fullWidth
                />
                <div className={"flex"}>
                    {loading ? (
                        <CircularProgress />
                    ) : (
                        <Button type="submit" variant={"contained"}>
                            VERIFY
                        </Button>
                    )}
                </div>
            </form>
        </Box>
    );
}
