import { Alert, Box, Button, CircularProgress, TextField } from "@mui/material";
import { useIsSmallScreen, useUser } from "../../context/AppContext";
import { FormEvent, useState } from "react";
import { Navigate } from "react-router-dom";
import { api } from "../../lib/api";
import queryString from "query-string";
import { decodeToken } from "../../lib/decode";

export default function Reset() {
    const [alert, setAlert] = useState<{
        severity: "success" | "error";
        message: string;
    }>({
        severity: "success",
        message: "",
    });
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useUser();
    const [password, setPassword] = useState("");

    const query = queryString.parse(window.location.search);
    const { email, token } = query;

    const isSmallScreen = useIsSmallScreen();

    if (user || !email || !token) return <Navigate to={"/"} replace />;

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        setLoading(true);
        setError("");
        api.post("/users/reset", {
            email,
            password,
            token,
        })
            .then((res) => {
                setLoading(false);
                localStorage.setItem("token", res.data.token);
                setUser(decodeToken(res.data.token));
            })
            .catch((err) => {
                setLoading(false);
                setAlert({
                    severity: "error",
                    message:
                        err?.response?.data?.error ||
                        err?.response?.data ||
                        "An error occurred. Please try again later.",
                });
            });
    };

    return (
        <Box className={"flex align-center flex-dir-column mb40"}>
            <form
                onSubmit={onSubmit}
                className={"flex flex-dir-column"}
                style={{ width: isSmallScreen ? "80vw" : "50vw" }}
            >
                <h2>Reset password</h2>
                {alert.message && (
                    <Alert className={"mb20"} severity={alert.severity}>
                        {alert.message}
                    </Alert>
                )}
                <TextField
                    className={"mb20"}
                    label={`New password`}
                    type={"password"}
                    onChange={(e) => {
                        setPassword(e.currentTarget.value);
                    }}
                    helperText={!password ? "Password must not be empty." : ""}
                    error={!!error}
                    disabled={loading}
                    variant={"outlined"}
                    required
                    fullWidth
                />
                <div className={"flex"}>
                    {loading ? (
                        <CircularProgress />
                    ) : (
                        <Button type="submit" variant={"contained"}>
                            Reset
                        </Button>
                    )}
                </div>
            </form>
        </Box>
    );
}
