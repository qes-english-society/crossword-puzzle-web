import { Alert, Box, CircularProgress, Tab, Tabs, Typography } from "@mui/material";
import { useCallback, useEffect, useState } from "react";
import { api } from "../lib/api";
import { SentimentDissatisfied } from "@mui/icons-material";
import RankTable from "../components/RankTable";
import React from "react";

export type result = { time: number; unsolved: number; id: number };
export type ranking = {
    result: result;
    email: string;
    class_name: string;
    class_number: string;
};
export type puzzle = { id: number; topic: string; short_topic: string };

export default function Rankings() {
    const [data, setData] = useState<ranking[]>([]);
    const [puzzles, setPuzzles] = useState<puzzle[]>([]);
    const [selected, setSelected] = useState<number>(0);
    const [noPlayers, setNoPlayers] = useState(false);

    const fetchData = useCallback(() => {
        setData([]);
        setNoPlayers(false);
        api.get(`/rank${!selected ? "" : `?puzzle=${selected}`}`).then((res) => {
            if (!res.data.length) return setNoPlayers(true);
            setData(res.data);
        });
    }, [selected]);

    useEffect(() => {
        api.get("/puzzles").then((res) => {
            setPuzzles(res.data.puzzles);
        });
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, selected]);

    return (
        <Box className={"flex flex-dir-column align-center"}>
            <h1 className={"text-decoration-underline"}>Rankings</h1>
            {!puzzles.length ? (
                <CircularProgress />
            ) : (
                <React.Fragment>
                    <Tabs
                        textColor="primary"
                        indicatorColor="primary"
                        variant="fullWidth"
                        onChange={(e, newValue) => setSelected(newValue)}
                        value={selected}
                        className={"mb20"}
                        sx={{
                            maxWidth: "90vw",
                            overflow: "auto",
                            [`& .MuiTabs-scroller`]: {
                                overflow: "auto !important",
                            },
                        }}
                    >
                        <Tab
                            label={"Best"}
                            color={"secondary"}
                            className={"text-transform-none"}
                            value={0}
                        />
                        {puzzles.map((puzzle) => (
                            <Tab
                                key={puzzle.id}
                                value={puzzle.id}
                                label={puzzle.short_topic}
                                className={"text-transform-none nowrap"}
                            />
                        ))}
                    </Tabs>
                    {!selected && (
                        <Alert severity={"info"} className={"mb20"}>
                            This is the best results of players, for reference only. We
                            calculate the rankings by puzzle.
                        </Alert>
                    )}
                    {data.length ? (
                        <RankTable
                            data={data}
                            puzzles={puzzles}
                            showPuzzleName={!selected}
                        />
                    ) : noPlayers ? (
                        <Typography className={"flex align-center font-size-20-force"}>
                            No one has played yet{" "}
                            <SentimentDissatisfied className={"ml5"} />
                        </Typography>
                    ) : (
                        <CircularProgress />
                    )}
                </React.Fragment>
            )}
        </Box>
    );
}
