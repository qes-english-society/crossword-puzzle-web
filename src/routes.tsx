import { Route, Routes as Switch } from "react-router-dom";
import Home from "./pages/Home";
import SignIn from "./pages/users/Signin";
import Register from "./pages/users/Register";
import Rank from "./pages/Rankings";
import Forgot from "./pages/users/Forgot";
import Reset from "./pages/users/Reset";

export default function Routes() {
    return (
        <Switch>
            <Route path="/" element={<Home />} />
            <Route path="/users/signin" element={<SignIn />} />
            <Route path="/users/register" element={<Register />} />
            <Route path={"/users/forgot"} element={<Forgot />} />
            <Route path={"/users/reset"} element={<Reset />} />
            <Route path={"/rank"} element={<Rank />} />
        </Switch>
    );
}
