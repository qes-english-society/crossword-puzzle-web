import "./App.css";
import "metahkg-css/dist/margin.min.css";
import "metahkg-css/dist/padding.min.css";
import "metahkg-css/dist/fontsize.min.css";
import React from "react";
import Routes from "./routes";
import ResponsiveAppBar from "./components/AppBar";
import { BrowserRouter as Router } from "react-router-dom";
import { Box } from "@mui/material";
import Theme from "./components/theme";
import { useWidth } from "./context/AppContext";

export default function App() {
    const [width] = useWidth();

    return (
        <Theme>
            <Box className="App">
                <Router>
                    <ResponsiveAppBar />
                    <Box
                        sx={{
                            overflow: "auto",
                            maxHeight: `calc(100vh - ${width < 600 ? 56 : 64}px)`,
                        }}
                    >
                        <Routes />
                    </Box>
                </Router>
            </Box>
        </Theme>
    );
}
