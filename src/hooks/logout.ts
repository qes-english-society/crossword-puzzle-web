import { useUser } from "../context/AppContext";

export const useLogout = () => {
    const [, setUser] = useUser();
    return () => {
        localStorage.removeItem("token");
        setUser(null);
    };
};
