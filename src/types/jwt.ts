import { Static, Type } from "@sinclair/typebox";

export const jwtTokenSchema = Type.Object({
    id: Type.Integer(),
    email: Type.String(),
    iss: Type.Literal(process.env.domain || ""),
    aud: Type.Literal(process.env.domain || ""),
});

export type jwtTokenType = Static<typeof jwtTokenSchema>;
