import { CluesInputOriginal } from "@jaredreisinger/react-crossword/dist/types";

// topic: ‘Together, We Fight the Virus!’
const health: CluesInputOriginal = {
    across: {
        "2": {
            clue: "Chief Executive Carrie Lam has confirmed that the next phase of Hong Kong’s Covid-19 ________ (n.) Pass will go ahead as scheduled on May 31.",
            answer: "VACCINE",
            row: 1,
            col: 10,
        },
        "6": {
            answer: "LOCKDOWN",
            clue: "a situation in which people are not allowed to enter or leave a building or area freely because of an emergency (n.)",
            row: 6,
            col: 0,
        },
        "7": {
            answer: "RECOVERY",
            clue: "the process of becoming well again after an illness or injury (n.)",
            row: 7,
            col: 8,
        },
        "10": {
            answer: "EPIDEMIC",
            clue: "a large number of cases of a particular disease happening at the same time in a particular community (n.)",
            row: 9,
            col: 3,
        },
        "12": {
            answer: "MASK",
            clue: "something that you wear over part or all of your face in order to protect it (n.)",
            row: 11,
            col: 10,
        },
        "13": {
            answer: "MORTALITY",
            clue: "the number of deaths in a particular situation or period of time (n.)",
            row: 12,
            col: 1,
        },
        "15": {
            answer: "WUHAN",
            clue: "Wang Hesheng was sent to ________ (n.), China, in early 2020 to rescue a public health system on the brink of collapse with the newly discovered coronavirus disease.",
            row: 14,
            col: 4,
        },
        "16": {
            answer: "QUARANTINE",
            clue: "Two government health advisers have said arrivals into Hong Kong who have received three doses of a Covid-19 vaccine should be allowed to ________ (v.) at home.",
            row: 17,
            col: 4,
        },
    },
    down: {
        "1": {
            answer: "ANTIBODY",
            clue: "a substance that the body produces in the blood to fight disease (n.)",
            row: 0,
            col: 15,
        },
        "3": {
            answer: "CORONAVIRUS",
            clue: "‘Together, We Fight the Virus!’ What is the ‘virus’?",
            row: 1,
            col: 12,
        },
        "4": {
            answer: "PANDEMIC",
            clue: "a disease that spreads over a whole country or the whole world (n.)",
            row: 3,
            col: 9,
        },
        "5": {
            answer: "ANOSMIA",
            clue: "the loss of the ability to detect one or more smells (n.)",
            row: 4,
            col: 5,
        },
        "8": {
            answer: "FEVER",
            clue: "a medical condition in which a person has a temperature that is higher than normal (n.)",
            row: 8,
            col: 3,
        },
        "9": {
            answer: "VENTILATOR",
            clue: "a piece of medical equipment with a pump that helps somebody to breathe by sending air in and out of their lungs (n.)",
            row: 8,
            col: 7,
        },
        "11": {
            answer: "IMMUNITY",
            clue: "________ (n.) is your body’s ability to protect you from getting sick when you are exposed to an infectious agent (“germ”) such as a bacterium, virus, parasite or fungus.",
            row: 11,
            col: 1,
        },
        "14": {
            answer: "HYGIENE",
            clue: "To prevent the spread of the disease, everyone should understand the importance of personal ________ (n.).",
            row: 13,
            col: 13,
        },
    },
};

// topic: ‘Together, We Go to School!’
const school: CluesInputOriginal = {
    across: {
        "4": {
            answer: "WHITEBOARD",
            clue: "Students should clean it before the teacher enters the classroom. (n.)",
            row: 2,
            col: 1,
        },
        "8": {
            answer: "CLUB",
            clue: "an organization for people who share an interest together (n.)",
            row: 5,
            col: 6,
        },
        "10": {
            answer: "ARITHMETIC",
            clue: "the type of mathematics that deals with the adding, multiplying, etc. of numbers (n.)",
            row: 8,
            col: 5,
        },
        "11": {
            answer: "SCHOLARSHIP",
            clue: "Martin won a ________ (n.) to study at Stanford.",
            row: 11,
            col: 9,
        },
        "13": {
            answer: "DISCIPLINE",
            clue: "QES has a reputation for high standards of ________ (n.).",
            row: 13,
            col: 6,
        },
        "15": {
            answer: "UNIFORM",
            clue: "The brown blazer is part of the school ________ (n.).",
            row: 15,
            col: 6,
        },
        "16": {
            answer: "KNOWLEDGE",
            clue: "the information that you gain through education or experience (n.)",
            row: 19,
            col: 0,
        },
    },
    down: {
        "1": {
            answer: "SUBJECT",
            clue: "Clara did well in every ________ (n.).",
            row: 0,
            col: 6,
        },
        "2": {
            answer: "DIVERSE",
            clue: "including many different types of people or things (adj.)",
            row: 1,
            col: 3,
        },
        "3": {
            answer: "CALCULATOR",
            clue: "I forgot to bring my ________ (n.) for the Maths exam today.",
            row: 1,
            col: 8,
        },
        "5": {
            answer: "PRINCIPAL",
            clue: "Mr Eric Chan is the ________ (n.) of QES.",
            row: 3,
            col: 13,
        },
        "6": {
            answer: "LEADERSHIP",
            clue: "Strong ________ (n.) is needed to captain the team.",
            row: 4,
            col: 11,
        },
        "7": {
            answer: "FRIENDSHIP",
            clue: "the state of being friends (n.)",
            row: 4,
            col: 17,
        },
        "9": {
            answer: "CAMPUS",
            clue: "Wi-fi is provided in many locations around the ________ (n.).",
            row: 7,
            col: 5,
        },
        "12": {
            answer: "PROTRACTOR",
            clue: "an instrument for measuring and drawing angles (n.)",
            row: 11,
            col: 19,
        },
        "14": {
            answer: "SCIENCE",
            clue: "________ (n.) class students will study Physics, Chemistry and Biology.",
            row: 13,
            col: 8,
        },
    },
};

// topic: 'Together, we immerse ourselves in popular culture!'
const popularCulture: CluesInputOriginal = {
    across: {
        "5": {
            answer: "INCEPTION",
            clue: "People are hired to enter the dreams of another person to implant ideas. Leonardo DiCaprio acts as the protagonist. Guess a movie. (n.)",
            row: 5,
            col: 3,
        },
        "9": {
            answer: "ALADDIN",
            clue: "A young boy who used the power from a genie in a magic lamp to make three wishes. He could travel by riding a flying mat.",
            row: 9,
            col: 0,
        },
        "11": {
            answer: "MEMORIES",
            clue: "A song that targets those 'who have ever experienced the loss of a loved one’. It received generally negative reviews from music critics and was listed as one of the worst songs of 2019 by Spin magazine.",
            row: 10,
            col: 9,
        },
        "13": {
            answer: "AVATAR",
            clue: "A creature with blue skin and is around three meters tall; another definition of this word is the image that represents you on online platforms or profiles. (n.)",
            row: 13,
            col: 0,
        },
        "15": {
            answer: "AVENGERS",
            clue: "the name of a team of superheroes who are committed to protecting the Earth from a variety of threats. (n.)",
            row: 16,
            col: 3,
        },
    },
    down: {
        "1": {
            answer: "FROZEN",
            clue: "‘Let it go’. Guess a movie.",
            row: 0,
            col: 11,
        },
        "2": {
            answer: "JOKER",
            clue: "A manic clown with antisocial personality.",
            row: 2,
            col: 6,
        },
        "3": {
            answer: "FLIPPED",
            clue: "An American romantic comedy-drama film that tells the story of two eighth graders who start to have feelings for each other, despite being total opposites. Guess a movie.",
            row: 3,
            col: 3,
        },
        "4": {
            answer: "FRAGILE",
            clue: "Being ______ mentally means being easily offended by certain words. A song released in Oct 2021 used this word as the name and Kimberly Chen was one of the two singers.",
            row: 4,
            col: 1,
        },
        "6": {
            answer: "TENET",
            clue: "A fictional organization has the power to make people travel backward in time; ‘What’s happened, happened.’; Christopher Nolan is the director. Guess a movie.",
            row: 5,
            col: 8,
        },
        "7": {
            answer: "RICKROLL",
            clue: "Beware of suspicious links that try to _________ (v.) you by redirecting you to the video of a certain song.",
            row: 6,
            col: 13,
        },
        "8": {
            answer: "SPEECHLESS",
            clue: "Unable to speak because you are so angry, shocked, surprised, etc. This is a song sung by Naomi Scott.",
            row: 7,
            col: 10,
        },
        "10": {
            answer: "INTERSTELLAR",
            clue: "Between the stars; Traveling among the stars; ‘Love is the one thing that transcends time and space.’ Guess a movie.",
            row: 9,
            col: 5,
        },
        "12": {
            answer: "BATMAN",
            clue: " A rich superhero who lives in a fictional city called Gotham. He hides his real identity from the public.",
            row: 12,
            col: 0,
        },
        "14": {
            answer: "TITANIC",
            clue: "a ship that hit an iceberg and sank; ‘You jump, I jump!’ Guess a movie.",
            row: 13,
            col: 3,
        },
    },
};

// console.log(JSON.stringify(health));
// console.log(JSON.stringify(school));
console.log(JSON.stringify(popularCulture));
